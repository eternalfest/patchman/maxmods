package maxmods;

import maxmods.add_link.AddLinkAction;
import maxmods.add_link.LinkInfo;

import hf.mode.GameMode;
import hf.levels.GameMechanics;
import hf.levels.Data in LevelData;
import hf.levels.TeleporterData;
import patchman.Ref;
import patchman.IPatch;
import merlin.IAction;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class AddLink {
  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  @:diExport
  public var action(default, null): IAction;

  private static var LINKS: WeakMap<TeleporterData, LinkInfo> = new WeakMap();
  private static var SAVED_LINKS: WeakMap<LevelData, Array<LinkInfo>> = new WeakMap();

  public function new() {
    this.patches = FrozenArray.of(
      // 'Revive' custom links when reloading a level
      Ref.auto(hf.levels.View.attach).after(function(hf, self) {
        var links = SAVED_LINKS.get(self.world.current);
        if (links == null) return;

        for (link in links) {
          var tp = getTeleporterAt(self.world, link.x, link.y);
          if (tp != null) LINKS.set(tp, link);
        }
      }),
        // Apply custom teleporter links
      Ref.auto(hf.levels.GameMechanics.getNextTeleporter).wrap(function(hf, self, start, old) {
        var link = LINKS.get(start);
        if (link == null) return old(self, start);

        var candidates = [];
        for (tp in self.teleporterList) {
          var dest = LINKS.get(tp);
          if (dest == null) continue;

          for (d in link.destinations) {
            if (dest.id == d) {
              candidates.push(tp);
              break;
            }
          }
        }

        return {
          fl_rand: candidates.length > 1,
          td: if (candidates.length == 0) {
            patchman.DebugConsole.warn('AddLink: no target for teleporter with id ${link.id} and destinations ${link.destinations}');
            null;
          } else { candidates[Std.random(candidates.length)]; },
        };
      })
    );

    this.action = new AddLinkAction(this);
  }

  public function addLink(game: GameMode, link: LinkInfo) {
    var tp = getTeleporterAt(game.world, link.x, link.y);
    if (tp != null) {
      LINKS.set(tp, link);
      var lvl = game.world.current;
      if (SAVED_LINKS.exists(lvl)) {
        SAVED_LINKS.get(lvl).push(link);
      } else {
        SAVED_LINKS.set(lvl, [link]);
      }
    }
  }

  private static function getTeleporterAt(world: GameMechanics, x: Int, y: Int): Null<TeleporterData> {
    var horiz = world.game.root.Data.HORIZONTAL;
    var verti = world.game.root.Data.VERTICAL;
    for (tp in world.teleporterList) {
      if ((tp.dir == horiz && x >= tp.cx && x < tp.cx + tp.length && tp.cy == y) ||
      (tp.dir == verti && y >= tp.cy && y < tp.cy + tp.length && tp.cx == x)) {
        return tp;
      }
    }

    patchman.DebugConsole.warn('AddLink: no teleporter at coordinates x=$x and y=$y');
    return null;
  }
}
