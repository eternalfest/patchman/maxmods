package maxmods.visual_effects;

import merlin.IActionContext;
import etwin.Obfu;
import merlin.IAction;

class Shake implements IAction {
    public var name(default, null): String = Obfu.raw("shake");
    public var isVerbose(default, null): Bool = false;

    private var mod: VisualEffects;

    public function new(mod: VisualEffects) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();

        var timer: Float = ctx.getOptFloat(Obfu.raw("t")).or(0);
        var intensity: Float = ctx.getOptFloat(Obfu.raw("n")).or(0);
        var flip: Bool = ctx.getOptBool(Obfu.raw("flip")).or(false);

        this.mod.setShake(game, timer, intensity, flip);

        return false;
    }
}
