package maxmods.visual_effects;

import merlin.IActionContext;
import etwin.Obfu;
import merlin.IAction;

class Zoom implements IAction {
    public var name(default, null): String = Obfu.raw("zoom");
    public var isVerbose(default, null): Bool = false;

    private var mod: VisualEffects;

    public function new(mod: VisualEffects) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();

        var cx: Float = ctx.getOptFloat(Obfu.raw("x")).or(0);
        var cy: Float = ctx.getOptFloat(Obfu.raw("y")).or(0);
        var x: Float = game.flipCoordReal(ctx.getOptInt(Obfu.raw("xr")).or(_ => ctx.getHf().Entity.x_ctr(cx)));
        var y: Float = ctx.getOptInt(Obfu.raw("yr")).or(_ => ctx.getHf().Entity.y_ctr(cy));

        var zoom: Float = ctx.getOptFloat(Obfu.raw("zoom")).toNullable();
        var flip: Bool = ctx.getOptBool(Obfu.raw("flip")).or(false);

        this.mod.zoom(game, x, y, zoom, flip);
        return false;
    }
}
