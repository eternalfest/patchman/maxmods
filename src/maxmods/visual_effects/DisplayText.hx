package maxmods.visual_effects;

import merlin.IActionContext;
import etwin.Obfu;
import merlin.IAction;

class DisplayText implements IAction {
    public var name(default, null): String = Obfu.raw("displayText");
    public var isVerbose(default, null): Bool = false;

    private var mod: VisualEffects;

    public function new(mod: VisualEffects) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();

        var cx: Float = ctx.getOptFloat(Obfu.raw("x")).or(0);
        var cy: Float = ctx.getOptFloat(Obfu.raw("y")).or(0);
        var x: Float = game.flipCoordReal(ctx.getOptInt(Obfu.raw("xr")).or(_ => ctx.getHf().Entity.x_ctr(cx)));
        var y: Float = ctx.getOptInt(Obfu.raw("yr")).or(_ => ctx.getHf().Entity.y_ctr(cy));

        var txt: String = ctx.getString(Obfu.raw("txt"));
        var textSize: Int = ctx.getOptInt(Obfu.raw("size")).or(100);

        this.mod.createText(game, x, y, textSize, txt);
        return false;
    }
}
