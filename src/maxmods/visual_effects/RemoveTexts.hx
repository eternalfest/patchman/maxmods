package maxmods.visual_effects;

import merlin.IActionContext;
import etwin.Obfu;
import merlin.IAction;

class RemoveTexts implements IAction {
    public var name(default, null): String = Obfu.raw("removeTexts");
    public var isVerbose(default, null): Bool = false;

    private var mod: VisualEffects;

    public function new(mod: VisualEffects) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        this.mod.removeTexts();
        return false;
    }
}
