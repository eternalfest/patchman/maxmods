package maxmods;

import maxmods.fixed_background.FixedBackgroundAction;

import merlin.IAction;
import patchman.IPatch;
import etwin.ds.FrozenArray;
import patchman.Ref;


@:build(patchman.Build.di())
class FixedBackground {
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    @:diExport
    public var action(default, null): IAction;

    public var background: Dynamic;
    public var leftBorder: Dynamic;
    public var rightBorder: Dynamic;

    public function new() {
        this.patches = FrozenArray.of(
            Ref.auto(hf.levels.ViewManager.onDataReady).after(function(hf, self) {
                if (self.fl_scrolling) {
                    leftBorder._visible = false;
                    rightBorder._visible = false;
                }
            }),
            Ref.auto(hf.levels.ViewManager.onScrollDone).before(function(hf, self) {
                leftBorder._visible = true;
                rightBorder._visible = true;
            }),
            Ref.auto(hf.levels.ViewManager.onRestoreReady).before(function(hf, self) {
                if (self.fl_fadeNextTransition) {
                    background.removeMovieClip();
                    leftBorder.removeMovieClip();
                    rightBorder.removeMovieClip();

                }
            })
        );
        this.action = new FixedBackgroundAction(this);
    }

    public function fixedBackground(game: hf.mode.GameMode, id: Int) {
        this.background.removeMovieClip();
        this.background = cast game.depthMan.attach("hammer_bg", game.root.Data.DP_SPECIAL_BG);
        this.background.gotoAndStop(Std.string(id));
        this.leftBorder.removeMovieClip();
        this.leftBorder = cast game.depthMan.attach("hammer_sides", game.root.Data.DP_SPECIAL_BG);
        this.leftBorder._x = -5;
        this.rightBorder.removeMovieClip();
        this.rightBorder = cast game.depthMan.attach("hammer_sides", game.root.Data.DP_SPECIAL_BG);
        this.rightBorder._x = game.root.Data.GAME_WIDTH + 5;
        return false;
    }
}