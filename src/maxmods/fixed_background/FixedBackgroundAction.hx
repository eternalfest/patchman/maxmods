package maxmods.fixed_background;

import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

class FixedBackgroundAction implements IAction {
    public var name(default, null): String = Obfu.raw("fixedBackground");
    public var isVerbose(default, null): Bool = false;

    private var mod: FixedBackground;

    public function new(mod: FixedBackground) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var id: Int = ctx.getInt(Obfu.raw("id"));

        this.mod.fixedBackground(game, id);

        return false;
    }
}
