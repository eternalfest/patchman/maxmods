package maxmods;

import maxmods.misc.DisableControls;
import maxmods.misc.DisablePause;
import maxmods.misc.InvertControls;
import maxmods.misc.SetShield;
import maxmods.misc.IsMusic;
import maxmods.misc.PlaySound;

import merlin.IAction;
import patchman.Ref;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import etwin.flash.Key;


@:build(patchman.Build.di())
class Misc {
    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var customDisabledControls: Bool = false;
    public var customDisabledPause: Bool = false;

    public function new() {
        this.actions = FrozenArray.of(
            new SetShield(this),
            new DisableControls(this),
            new DisablePause(this),
            new InvertControls(this),
            new IsMusic(this),
            new PlaySound(this)
        );

        var patches = [
            Ref.auto(hf.entity.PlayerController.getControls).wrap(function(hf: hf.Hf, self: hf.entity.PlayerController, old): Void {
                if (this.customDisabledControls == false) {
                    old(self);
                }
            }),
            Ref.auto(hf.mode.GameMode.onPause).wrap(function(hf: hf.Hf, self: hf.mode.GameMode, old): Void {
                if (this.customDisabledPause == false) {
                    old(self);
                }
            }),
            Ref.auto(hf.mode.GameMode.onMap).wrap(function(hf: hf.Hf, self: hf.mode.GameMode, old): Void {
                if (this.customDisabledPause == false) {
                    old(self);
                }
            }),
        ];
        this.patches = FrozenArray.from(patches);
    }

    public function setShield(game: hf.mode.GameMode, t: Int) {
        var players = game.getPlayerList();
        for (i in 0...players.length) {
            var player = players[i];
            if (player.fl_shield)
                player.shield(t);
        }
    }

    public function setDisabledControls(game: hf.mode.GameMode, active: Bool) {
        this.customDisabledControls = active;
        game.fl_disguise = !active;
        game.root.Data.WAIT_TIMER = (active ? game.root.Data.SECOND * 99999 : game.root.Data.SECOND * 8);
        var player = game.getPlayerList()[0];
        player.dx = 0;
        if (player.animId == player.baseWalkAnim.id || player.animId == game.root.Data.ANIM_PLAYER_RUN.id) {
            player.playAnim(player.baseStopAnim);
        }
    }

    public function setDisabledPause(game: hf.mode.GameMode, active: Bool) {
        this.customDisabledPause = active;
    }

    public function setInvertedControls(game: hf.mode.GameMode, active: Bool): Void {
        var solo_igor = {up: Key.UP, down: Key.DOWN, left: Key.LEFT, right: Key.RIGHT, bomb: Key.SPACE};
        var multi_igor = {up: Key.UP, down: Key.DOWN, left: Key.LEFT, right: Key.RIGHT, bomb: Key.ENTER};
        var multi_sandy = {up: 82, down: 70, left: 68, right: 71, bomb: 65};

        if (active) {
            game.getPlayerList()[0].ctrl.setKeys(solo_igor.up, solo_igor.down, solo_igor.right, solo_igor.left, solo_igor.bomb);
            if (game.getPlayerList().length > 1) {
                game.getPlayerList()[0].ctrl.setKeys(multi_igor.up, multi_igor.down, multi_igor.right, multi_igor.left, multi_igor.bomb);
                game.getPlayerList()[1].ctrl.setKeys(multi_sandy.up, multi_sandy.down, multi_sandy.right, multi_sandy.left, multi_sandy.bomb);
            }
        }
        else {
            game.getPlayerList()[0].ctrl.setKeys(solo_igor.up, solo_igor.down, solo_igor.left, solo_igor.right, solo_igor.bomb);
            if (game.getPlayerList().length > 1) {
                game.getPlayerList()[0].ctrl.setKeys(multi_igor.up, multi_igor.down, multi_igor.left, multi_igor.right, multi_igor.bomb);
                game.getPlayerList()[1].ctrl.setKeys(multi_sandy.up, multi_sandy.down, multi_sandy.left, multi_sandy.right, multi_sandy.bomb);
            }
        }
    }

    public function isMusic(game: hf.mode.GameMode, orSound: Bool) {
        if (game.root.GameManager.CONFIG.hasMusic() && game.fl_mute == false) {
            return true;
        }
        if (orSound && game.root.GameManager.CONFIG.soundVolume > 0) {
            return true;
        }
        return false;
    }

    public function playSound(game: hf.mode.GameMode, sound: String) {
        if ((game.root.GameManager.CONFIG.hasMusic() && game.fl_mute == false) || game.root.GameManager.CONFIG.soundVolume > 0) {
            game.soundMan.playSound(sound, 7);
        }
    }
}
