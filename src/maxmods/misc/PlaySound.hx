package maxmods.misc;

import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

class PlaySound implements IAction {
    public var name(default, null): String = Obfu.raw("playSound");
    public var isVerbose(default, null): Bool = false;

    private var mod: Misc;

    public function new(mod: Misc) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var sound: String = ctx.getString(Obfu.raw("n"));

        this.mod.playSound(game, sound);

        return false;
    }
}
