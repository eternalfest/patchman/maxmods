package maxmods.misc;

import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

class SetShield implements IAction {
  public var name(default, null): String = Obfu.raw("setShield");
  public var isVerbose(default, null): Bool = false;

  private var mod: Misc;

  public function new(mod: Misc) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: hf.mode.GameMode = ctx.getGame();
    var t: Int = ctx.getInt(Obfu.raw("t"));

    this.mod.setShield(game, t);

    return false;
  }
}
