package maxmods.misc;

import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

class DisablePause implements IAction {
    public var name(default, null): String = Obfu.raw("disablePause");
    public var isVerbose(default, null): Bool = false;

    private var mod: Misc;

    public function new(mod: Misc) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var active: Bool = ctx.getBool(Obfu.raw("n"));

        this.mod.setDisabledPause(game, active);

        return false;
    }
}
