package maxmods.misc;

import patchman.DebugConsole;
import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

class IsMusic implements IAction {
    public var name(default, null): String = Obfu.raw("isMusic");
    public var isVerbose(default, null): Bool = false;

    private var mod: Misc;

    public function new(mod: Misc) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var orSound: Bool = ctx.getOptBool(Obfu.raw("orSound")).or(false);

        return this.mod.isMusic(game, orSound);
    }
}
