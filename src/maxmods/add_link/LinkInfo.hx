package maxmods.add_link;

typedef LinkInfo = {
  id: String,
  x: Int,
  y: Int,
  destinations: Array<String>,
}