package maxmods.add_link;

import hf.mode.GameMode;
import etwin.Obfu;
import merlin.IActionContext;
import merlin.IAction;

class AddLinkAction implements IAction{
  public var name(default, null): String = Obfu.raw("addLink");
  public var isVerbose(default, null): Bool = false;

  private var mod: AddLink;

  public function new(mod: AddLink) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var link: LinkInfo = {
      x: Std.int(game.flipCoordCase(ctx.getInt(Obfu.raw("x")))),
      y: ctx.getInt(Obfu.raw("y")),
      id: ctx.getString(Obfu.raw("id")),
      destinations: ctx.getString(Obfu.raw("to")).split(",")
    };

    this.mod.addLink(game, link);

    return false;
  }
}
