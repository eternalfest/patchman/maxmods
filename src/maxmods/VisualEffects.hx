package maxmods;

import maxmods.visual_effects.DisplayText;
import maxmods.visual_effects.RemoveTexts;
import maxmods.visual_effects.Zoom;
import maxmods.visual_effects.Shake;

import etwin.flash.MovieClip;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;

@:build(patchman.Build.di())
class VisualEffects {

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var textList: Array<MovieClip>;
    public var customZoom: Bool;
    public var customShakeTimerTotal: Float;
    public var customShakeTimer: Float;
    public var customShakeIntensity: Float;
    public var customShakeFlip: Bool;

    public function new() {
        this.actions = FrozenArray.of(
            new DisplayText(this),
            new RemoveTexts(this),
            new Zoom(this),
            new Shake(this)
        );

        this.textList = new Array();

        var patches = [
            Ref.auto(hf.mode.GameMode.clearLevel).before(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                this.removeTexts();
            }),

            Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                if (this.customShakeTimer > 0) {
                    if (this.customShakeFlip && Std.random(2) == 1) {
                        self.mc._xscale *= -1;
                    }
                    if (this.customShakeFlip && Std.random(2) == 1) {
                        self.mc._yscale *= -1;
                    }
                    this.customShakeTimer -= self.root.Timer.tmod;
                }
                if (this.customShakeTimer <= 0) {
                    this.customShakeTimer = 0;
                    this.customShakeIntensity = 0;
                    this.customShakeFlip = false;
                    if (this.customZoom != true) {
                        self.mc._xscale = 100;
                        self.mc._yscale = 100;
                    }
                }
                if (this.customZoom != true) {
                    self.mc._x = Math.round(self.xOffset + (Std.random(2) * 2 - 1) * (Std.random(Math.round(this.customShakeIntensity * 10)) / 10)) + (self.mc._xscale < 0 ? self.root.Data.DOC_WIDTH - 20: 0);
                    self.mc._y = Math.round(self.yOffset + (Std.random(2) * 2 - 1) * (Std.random(Math.round(this.customShakeIntensity * 10)) / 10)) + (self.mc._yscale < 0 ? self.root.Data.DOC_HEIGHT : 0);
                }

            }),
        ];
        this.patches = FrozenArray.from(patches);
    }

    public function createText(game: hf.mode.GameMode, x: Float, y: Float, textSize: Float, txt: String): Void {
        var hf: hf.Hf = game.root;
        for(text in textList) {
            if ((cast text).field.text == txt) {
                return null;
            }
        }
        var Text: MovieClip = cast game.depthMan.attach('hammer_interf_inGameMsg', hf.Data.DP_SPRITE_BACK_LAYER);

        (cast Text).label.text = "";
        (cast Text).field._width = 400;
        (cast Text).field.text = txt;
        (cast Text).field._xscale = textSize;
        (cast Text).field._yscale = textSize;
        (cast Text).field._x = x;
        (cast Text).field._y = y;
        if (game.fl_mirror) {
            (cast Text).field._x -= (cast Text).field.textWidth * ((cast Text).field._xscale / 100);
        }
        hf.FxManager.addGlow((cast Text), 0, 2);
        textList.push((cast Text));
    }

    public function removeTexts() {
        for(i in 0...textList.length) {
            textList[i].removeMovieClip();
        }
        textList.splice(0, textList.length);
    }

    public function zoom(game: hf.mode.GameMode, x: Float, y: Float, zoom: Float, flip: Bool): Void {
        if (zoom == null) {
            game.mc._x = 10;
            game.mc._y = 0;
            game.mc._xscale = 100;
            game.mc._yscale = 100;
            this.customZoom = false;
        } else {
            game.mc._x = 10 - (zoom - 1) * x - x + 210 + zoom * 10;
            game.mc._y = -(zoom - 1) * y - y + 260;
            game.mc._xscale = zoom * 100;
            game.mc._yscale = zoom * 100;
            if (flip) {
                game.mc._y = 500 - game.mc._y;
                game.mc._yscale *= -1;
            }
            this.customZoom = true;
        }
    }

    public function setShake(game: hf.mode.GameMode, timer: Float, intensity: Float, flip: Bool): Void {
        this.customShakeTimerTotal = timer;
        this.customShakeTimer = timer;
        this.customShakeIntensity = intensity;
        this.customShakeFlip = flip;
    }
}
