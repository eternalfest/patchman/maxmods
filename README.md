# @patchman/maxmods

Mods by maxdefolsch.

* `AddLink` : sets custom links between teleporters via script.
* `FixedBackground` : forces the background to stay static during level transitions.
* `VisualEffects` :
  * `DisplayText` / `RemoveTexts` : displays static text sprites via script.
  * `Shake` : shakes the level via script.
  * `Zoom` : zooms in on a portion of the level via script.
* `Misc` :
  * `DisableControls` : disables player controls via script.
  * `DisablePause` : disables pause/map via script.
  * `InvertControls` : inverts left/right controls via script.
  * `IsMusic` : tests if music is activated via script.
  * `PlaySound` : plays a sound via script.
  * `SetShield` : sets the player's shield via script.