# 0.3.0 (2023-03-04)

- **[Breaking change]** Update to merlin 0.17.0.

# 0.2.18 (2021-12-19)

- **[Fix]** `DisableControls` now stops the player from moving.

# 0.2.17 (2021-11-04)

- **[Feature]** Add `DisablePause` to `Misc`.
- **[Feature]** Add list of mods to `README.txt`.

# 0.2.16 (2021-06-11)

- **[Fix]** `FixedBackground` now somewhat works with dimensions.

# 0.2.15 (2021-05-13)

- **[Feature]** Move sound checking logic to `PlaySound`.

# 0.2.14 (2021-05-03)

- **[Fix]** Disable disguises with `DisableControls`.

# 0.2.13 (2021-05-01)

- **[Fix]** Add missing Key import to `Misc`.

# 0.2.12 (2021-05-01)

- **[Feature]** Add `InvertControls` to `Misc`.

# 0.2.11 (2021-04-30)

- **[Feature]** Add `Shake` to `VisualEffects`.

# 0.2.10 (2021-04-29)

- **[Feature]** Add `PlaySound` to `Misc`.

# 0.2.9 (2021-04-29)

- **[Feature]** Add `IsMusic` to `Misc`.

# 0.2.8 (2021-04-26)

- **[Fix]** Fix width of sprite in `DisplayText`.

# 0.2.7 (2021-04-26)

- **[Feature]** Add `DisableControls` to `Misc`.

# 0.2.6 (2021-04-26)

- **[Internal]** Move `SetShield` to `Misc` package.

# 0.2.5 (2021-04-26)

- **[Internal]** Rework coordinate extraction for `DisplayText` and `Zoom`.

# 0.2.4 (2021-04-26)

- **[Fix]** Fix xr/yr in Mirror for `DisplayText`.
- **[Feature]** Add `Zoom` to `VisualEffects`.

# 0.2.3 (2021-04-26)

- **[Fix]** Fix x/y use for `DisplayText`.
- **[Feature]** Make `DisplayText` work with the Mirror option.

# 0.2.2 (2021-04-22)

- **[Feature]** Create `visual_effects.VisualEffects` mod with `DisplayText`/`RemoveTexts`.

# 0.2.1 (2021-04-22)

- **[Breaking change]** Update to `patchman@0.10.4`.
- **[Breaking change]** Reorganize classes.
  - `add_link.AddLink` is moved to `maxmods.AddLink`.
  - `fixed_background.FixedBackground` is moved to `maxmods.FixedBackground`.
  - `set_shield.SetShield` is moved to `maxmods.SetShield`.
- **[Internal]** Update to Yarn 2.

# 0.1.1 - 0.1.6

- **[Feature]** Add `set_shield.SetShield` mod.
- **[Feature]** Add `fixed_background.FixedBackground` mod.

# 0.1.0

- **[Feature]** First release.
- **[Feature]** Add `add_link.AddLink` mod.
